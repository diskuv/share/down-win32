/*---------------------------------------------------------------------------
   Copyright (c) 2019 The down programmers. All rights reserved.
   Distributed under the ISC license, see license at the end of the file.
   down release v0.1.0
   --------------------------------------------------------------------------*/

#include <stdbool.h>
#ifdef _WIN32
#include <windows.h>
#include <wchar.h>
#else
#include <termios.h>
#include <unistd.h>
#endif
#include <errno.h>
#include <signal.h>
#include <caml/mlvalues.h>
#include <caml/memory.h>

#ifdef _WIN32
/* Windows support functions described in detail at
 * https://learn.microsoft.com/en-us/windows/console/console-virtual-terminal-sequences
 * under MIT license https://github.com/MicrosoftDocs/Console-Docs/blob/main/LICENSE-CODE */

DWORD fdwSaveOldMode;
bool haveSavedOldMode;

bool EnableRawMode()
{
    /* Set output mode to handle virtual terminal sequences
     * and to output window size changes */
    HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
    if (hOut == INVALID_HANDLE_VALUE) {
        return false;
    }

    DWORD dwMode = 0;
    if (!GetConsoleMode(hOut, &dwMode)) {
        return false;
    }

    /* Save output mode */
    if (!haveSavedOldMode) {
        fdwSaveOldMode = dwMode;
        haveSavedOldMode = true;
    }

    dwMode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING | ENABLE_WINDOW_INPUT;
    if (!SetConsoleMode(hOut, dwMode)) {
        return false;
    }
    return true;
}
bool RestoreMode() {
    if (!haveSavedOldMode) {
        return true;
    }

    HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
    if (hOut == INVALID_HANDLE_VALUE) {
        return false;
    }

    if (!SetConsoleMode(hOut, fdwSaveOldMode)) {
        return false;
    }
    haveSavedOldMode = false;
    return true;
}
bool IsCtrlZPressed(const INPUT_RECORD* ir) {
  return (
    (ir->Event.KeyEvent.dwControlKeyState & (LEFT_CTRL_PRESSED | RIGHT_CTRL_PRESSED) != 0)
    && ir->Event.KeyEvent.wVirtualKeyCode == 'Z');
}
#endif

CAMLprim value ocaml_down_stdin_set_raw_mode (value set_raw)
{
  CAMLparam1 (set_raw);
  static bool is_raw = false;
#ifdef _WIN32
  if (Bool_val (set_raw)) {
    if (!is_raw) {
      if (EnableRawMode()) { is_raw = true; }
      else { CAMLreturn (Val_bool(0)); }
    }
  } else {
    if (is_raw) {
      if (RestoreMode()) { is_raw = false; }
      else { CAMLreturn (Val_bool(0)); }
    }
  }
#else
  static struct termios restore = {0};
  struct termios set;

  if (Bool_val (set_raw)) {
    if (!is_raw) {
      if (!isatty (0)) { CAMLreturn (Val_bool (0)); }
      if (tcgetattr (0, &restore) < 0) { CAMLreturn (Val_bool (0)); }
      set = restore;
      set.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
      set.c_oflag &= ~(OPOST);
      set.c_cflag |= (CS8);
      set.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
      set.c_cc[VMIN] = 1; set.c_cc[VTIME] = 0;
      if (tcsetattr (0, TCSAFLUSH, &set) < 0) { CAMLreturn (Val_bool (0)); }
      else { is_raw = true; };
    }
  } else {
    if (is_raw) {
      if (tcsetattr (0, TCSAFLUSH, &restore) < 0) { CAMLreturn (Val_bool (0)); }
      else { is_raw = false; }
    }
  }
#endif
  CAMLreturn (Val_bool (1));
}

CAMLprim value ocaml_down_stdin_readc (value unit)
{
  CAMLparam1 (unit);
#ifdef _WIN32
  static char utf8Buf[4] = {0, 0, 0, 0};
  static char* utf8Pos = NULL;
  static int utf8Remaining = 0;
  DWORD cNumRead;
  INPUT_RECORD irInBuf[1];
  HANDLE hStdin;
  wchar_t wchBuf[2] = { 0, 0 };
  unsigned char buf;
  int i; int ret;
  /* zip through remainder of any UTF-8 bytes */
  if(utf8Remaining > 0 && utf8Pos) {
    buf = *utf8Pos;
    utf8Pos++;
    utf8Remaining--;
    CAMLreturn (Val_int (buf));
  }
  /* read one Unicode character */
  hStdin = GetStdHandle(STD_INPUT_HANDLE);
  if (hStdin == INVALID_HANDLE_VALUE) {
    CAMLreturn (Val_int (-3));
  }
  while (!wchBuf[0]) {
    if (!ReadConsoleInputW(
            hStdin,
            irInBuf,
            1,         /* max size of read buffer */
            &cNumRead)) {
      CAMLreturn (Val_int(-3));
    }
    for (i = 0; i < cNumRead; i++) {
      switch(irInBuf[i].EventType) {
        case FOCUS_EVENT:  /* disregard focus events */
        case MENU_EVENT:  /* disregard menu events */
        case MOUSE_EVENT:  /* disregard mouse events */
        case WINDOW_BUFFER_SIZE_EVENT:  /* disregard window size events (for now!) */
          break;
        case KEY_EVENT:
          if (irInBuf[i].Event.KeyEvent.bKeyDown) { /* key pressed */
            if (IsCtrlZPressed (irInBuf + i)) {
              /* Convention on Windows is to interpret Ctrl-Z as EOF */
              CAMLreturn (Val_int (-1));
            }
            if (!irInBuf[i].Event.KeyEvent.uChar.UnicodeChar) {
              continue;
            }
            wchBuf[0] = irInBuf[i].Event.KeyEvent.uChar.UnicodeChar;
            break;
          }
      }
    }
  }
  /* convert to UTF-8 */
  ret = WideCharToMultiByte(CP_UTF8, 0,
                      wchBuf, 1 /* 1 wide character */,
                      utf8Buf, sizeof(utf8Buf) /* 4 bytes max */,
                      NULL, NULL);
  if (!ret) { CAMLreturn (Val_int (-3)); }
  if (ret < 0 || ret > sizeof(utf8Buf)) { CAMLreturn (Val_int (-3)); }
  utf8Pos = utf8Buf;
  utf8Remaining = ret;
  /* take and return one byte from pending buffer */
  buf = *utf8Pos;
  utf8Pos++;
  utf8Remaining--;
  CAMLreturn (Val_int (buf));
#else
  int ret; unsigned char buf;
  ret = read(0, &buf, 1);
  if (ret == 1) { CAMLreturn (Val_int (buf)); };
  if (ret == 0) { CAMLreturn (Val_int (-1)); };
  if (ret == -1 && errno == EINTR) { CAMLreturn (Val_int (-2)); };
  CAMLreturn (Val_int (-3));
#endif
}

CAMLprim value ocaml_down_sigwinch (value unit)
{
  CAMLparam1 (unit);
#ifdef _WIN32
  CAMLreturn (Val_int (-1));
#else
  CAMLreturn (Val_int (SIGWINCH));
#endif
}

/*---------------------------------------------------------------------------
   Copyright (c) 2019 The down programmers

   Permission to use, copy, modify, and/or distribute this software for any
   purpose with or without fee is hereby granted, provided that the above
   copyright notice and this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
   WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
   ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
   ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
   OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
  ---------------------------------------------------------------------------*/
